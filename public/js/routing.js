/**
 * Created by Sergey on 24.02.2016.
 */

lendProject.config(function($stateProvider, $urlRouterProvider){
    $stateProvider

        .state('auth', {
            abstract: true,
            url: '/auth',
            templateUrl: "auth.html"
        })
        .state('auth.login', {
            parent: "auth",
            url: "/login",
            templateUrl: "login.html",
            controller: "loginCtrl"
        })
        .state('auth.register', {
            parent: "auth",
            url: "/register",
            templateUrl: "register.html"
        })

        .state('main', {
            abstract: true,
            url: "/",
            templateUrl: "partials/main.html"
            //controller: "mainCtrl"
            // controllerAs: 'vm'
        })
        .state('main.content', {
            url: "results",
            templateUrl: "partials/showResults.html",
            controller: "mainCtrl"
        })
        .state('main.carDetails', {
            url: "/details/:carId",
            templateUrl: "partials/carDetails.html",
            controller: "viewCarDetailsCtrl",
            controllerAs: 'vm',            
            params: {
                carId: undefined
            }
        })
        .state('main.carInfo', {
            url: "/info/:carId",
            templateUrl: "partials/carInfo.html",
            controller: "viewCarInfoCtrl",
            controllerAs: 'vm',            
            params: {
                carId: undefined
            }
        })

        
        .state('main.search', {
            url: "search",
            templateUrl: "partials/search.html"
        })
        .state('main.deals', {
            url: "deals",
            templateUrl: "deals.html"
        })
        .state('main.contacts', {
            url: "contacts",
            templateUrl: "contacts.html"
        })

        //Admin panel
        .state('admin', {
            url: "/admin",
            abstract: true,
            controller: "adminCtrl",
            templateUrl: "adminPanel/partials/adminTemplate.html"
            // controllerAs: 'vm'            
        })

        .state('admin.dashboard', {
            url: "/dashboard",
            templateUrl: "adminPanel/partials/dashboard.html"
        })

        .state('admin.charts', {
            url: "/charts",
            templateUrl: "adminPanel/partials/charts.html"
        })

        .state('admin.tables', {
            url: "/tables",
            templateUrl: "adminPanel/partials/tables.html"
        });

    $urlRouterProvider.otherwise('/auth/login');
});