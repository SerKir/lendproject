/**
 * Created by Sergey on 24.02.2016.
 */

(function() {

    lendProject
        .controller('loginCtrl', loginCtrl) 
        .controller('mainCtrl', mainCtrl)
        .controller('datePickerCtrl', datePickerCtrl)
        .controller('getLocationsCtrl',getLocationsCtrl)
        .controller('viewCarDetailsCtrl', viewCarDetailsCtrl)
        .controller('viewCarInfoCtrl', viewCarInfoCtrl)
        .controller('advancedSearchCtrl', advancedSearchCtrl)
        .controller('adminCtrl', adminCtrl)
        // .controller('searchPanelCtrl', searchPanelCtrl)
        .directive('searchPanel', searchPanel)
        .directive('navbar', navbar);

    function loginCtrl($scope, $location, $state){
        console.log("Hello from LOGIN controller");
        $scope.email = "";
        $scope.password = "";
        $scope.errorMessage;

        $scope.submit = function(){
            if($scope.email == '1@1.com' && $scope.password == '11111'){
                $state.go('main.search'); 
            } else {
                $("[data-hide]").show();
                $scope.errorMessage = "Incorrect email or password";
                console.log(" incorrect. Email " + $scope.loginEmail + "; password " + $scope.password);
            }
        };

        $(function(){
            $("[data-hide]").on("click", function(){
                $(this).closest("." + $(this).attr("data-hide")).hide();
            });
        });
    }

    function mainCtrl($scope, $http, $stateParams) {
        console.log("Hello from MAIN controller");

        $http.get('/results')
            .then(
                function(response){
                    // Get data from server
                    $scope.cars = response.data;

                    // Pagination
                    $scope.totalItems = $scope.cars.length;
                    $scope.currentPage = 1;
                    $scope.maxSize = 9;

                    $scope.setPage = function (pageNumber) {
                        $scope.currentPage = pageNumber;
                    };

                    $scope.$watch('currentPage', function (num) {
                        var begin = (($scope.currentPage - 1) * $scope.maxSize),
                            end = begin + $scope.maxSize;
                        $scope.filteredCars = $scope.cars.slice(begin, end);
                    });
                    // End of pagination

                }, 	function(response){
                    console.log("Error getting response: " + response);
                }
            );


        if($stateParams.carId !== undefined){
            $scope.car = $scope.cars[$stateParams.carId];
        }

        $scope.isCollapsed = true;
    }

    function datePickerCtrl($scope, $http) {

        // Date picker
        $scope.today = function() {
            $scope.dt = new Date();
        };

        $scope.today();                 // on button click Today - shows today's date on calendar
        $scope.clear = function() {     // on button click Clear - clears the selected date
            $scope.dt = null;
        };

        $scope.dateOptions = {
            maxDate: new Date(2025, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        $scope.open = function() {
            $scope.popup.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        $scope.popup = {
            opened: false
        };

        // Time picker
        $scope.selectedTime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 1;
    }

    function getLocationsCtrl($scope, $http){

        var _selected;

        $scope.selected = undefined;

        $scope.getLocation = function(val) {
            return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(function(response){
                return response.data.results.map(function(item){
                    return item.formatted_address;
                });
            });
        };

        $scope.ngModelOptionsSelected = function(value) {
            if (arguments.length) {
                _selected = value;
            } else {
                return _selected;
            }
        };

        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
    }

    function viewCarDetailsCtrl($scope, $stateParams){

        // Display car details - image, name, description
        if($stateParams.carId !== undefined){
            $scope.car = $scope.cars[$stateParams.carId];
        }

        // SLIDER CONTROLLER
        $scope.myInterval = 4000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        var slides = $scope.slides = [];
        var currIndex = 0;

        $scope.addSlide = function() {
            var newWidth = 600 + slides.length + 1;
            slides.push({
                image: 'http://lorempixel.com/' + newWidth + '/300',
                text: ['Nice image','Awesome photograph','That is so cool','I love that'][slides.length % 4],
                id: currIndex++
            });
        };

        $scope.randomize = function() {
            var indexes = generateIndexesArray();
            assignNewIndexesToSlides(indexes);
        };

        for (var i = 0; i < 6; i++) {
            $scope.addSlide();
        }

        // Randomize logic below
        function assignNewIndexesToSlides(indexes) {
            for (var i = 0, l = slides.length; i < l; i++) {
                slides[i].id = indexes.pop();
            }
        }

        function generateIndexesArray() {
            var indexes = [];
            for (var i = 0; i < currIndex; ++i) {
                indexes[i] = i;
            }
            return shuffle(indexes);
        }

        function shuffle(array) {
            var tmp, current, top = array.length;
            if (top) {
                while (--top) {
                    current = Math.floor(Math.random() * (top + 1));
                    tmp = array[current];
                    array[current] = array[top];
                    array[top] = tmp;
                }
            }
            return array;
        }
    }

    function viewCarInfoCtrl($scope, $stateParams){
        if($stateParams.carId !== undefined){
            $scope.car = $scope.cars[$stateParams.carId];
        }
    }

    function advancedSearchCtrl($scope){
        $scope.isCollapsed = false;
    }

    function adminCtrl($scope){
        // to be created
    }    

    // function searchPanelCtrl($scope){
    //     $scope.isCollapsed = true;
    // }


    function searchPanel(){
        return {
            templateUrl : 'partials/searchPanel.html'
        };
    }

    function navbar(){
        return {
            templateUrl : 'partials/navbar.html'
        };
    };


})();