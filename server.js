var express = require('express');
var app = express();
var port = 4000;

app.use(express.static(__dirname + "/public"));

app.get('/results', function(req, res){
	console.log("I am server. I received a GET request...");

	var cars = [
	    {
	        id:1,
	        image: 'img/cars/car1.jpg',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:2,
	        image: 'img/cars/car2.jpg',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:3,
	        image: 'img/cars/car3.jpg',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:4,
	        image: 'img/cars/car4.jpg',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:5,
	        image: 'img/cars/car5.jpg',
	        model: 'VW',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:6,
	        image: 'img/cars/car6.jpg',
	        model: 'Range Rover',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:7,
	        image: 'img/cars/car7.png',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:8,
	        image: 'img/cars/car8.jpg',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:9,
	        image: 'img/cars/car9.png',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:10,
	        image: 'img/cars/car10.jpg',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:11,
	        image: 'img/cars/car11.jpg',
	        model: 'VW',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:12,
	        image: 'img/cars/car12.jpg',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:13,
	        image: 'img/cars/car13.jpg',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:14,
	        image: 'img/cars/car14.jpg',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:15,
	        image: 'img/cars/car13.jpg',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:16,
	        image: 'img/cars/car12.jpg',
	        model: 'VW',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:17,
	        image: 'img/cars/car11.jpg',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:18,
	        image: 'img/cars/car12.jpg',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:19,
	        image: 'img/cars/car13.jpg',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:20,
	        image: 'img/cars/car10.jpg',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:21,
	        image: 'img/cars/car9.png',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:22,
	        image: 'img/cars/car12.jpg',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:23,
	        image: 'img/cars/car13.jpg',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:24,
	        image: 'img/cars/car14.jpg',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:25,
	        image: 'img/cars/car5.jpg',
	        model: 'VW',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:26,
	        image: 'img/cars/car6.jpg',
	        model: 'Range Rover',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:27,
	        image: 'img/cars/car1.jpg',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:28,
	        image: 'img/cars/car2.jpg',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:29,
	        image: 'img/cars/car3.jpg',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:30,
	        image: 'img/cars/car4.jpg',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:31,
	        image: 'img/cars/car5.jpg',
	        model: 'VW',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:32,
	        image: 'img/cars/car10.jpg',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:33,
	        image: 'img/cars/car11.jpg',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:34,
	        image: 'img/cars/car12.jpg',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:35,
	        image: 'img/cars/car13.jpg',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:36,
	        image: 'img/cars/car14.jpg',
	        model: 'VW',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:37,
	        image: 'img/cars/car6.jpg',
	        model: 'Audi',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:38,
	        image: 'img/cars/car7.png',
	        model: 'Honda',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:39,
	        image: 'img/cars/car8.jpg',
	        model: 'Maseratti',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:40,
	        image: 'img/cars/car9.png',
	        model: 'Opel',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    },
	    {
	        id:41,
	        image: 'img/cars/car10.jpg',
	        model: 'VW',
	        description: 'Amazingly rare, these are some of the most sought after Ford GTs that were ever built. ' +
	        'Now, one is for sale in the Middle East at Topline Motors'
	    }
	];

	res.json(cars);
});

app.listen(port);
console.log("Server running on port " + port);


